#' Miscellaneous Functions for the Forest Research Institute of the
#' State Baden-Wuerttemberg
#'
#' Package <https://CRAN.R-project.org/package=fritools> may not import
#' other than core R. frimisc may.
#'
#' You will find the details in\cr \code{vignette("An_Introduction_to_frimisc",
#' package = "frimisc")}.
#'
#' @name frimisc-package
#' @aliases frimisc-package
#' @docType package
#' @keywords package
NULL

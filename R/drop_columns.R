drop_columns <- function(x, column_names = NA) {
    res <- x[ , ! names(x) %in% column_names]
    return(res)
}

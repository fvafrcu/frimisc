# frimisc 0.6.0.9000

* Add `drop_columns()` and `git_compare_cleaned_csv()`.

# frimisc 0.6.0

* Added `send_mail()`.
* Added git wrappers for `gert`.
* `is_probaly_at_fva()` works with new notebooks.
* `fake_directory()` now returns `tempdir()` if not running on known machines.

# frimisc 0.5.0

* Renamed argument `mustWork` to `fake_path()` to `must_work`.
* `fake_path()` now has a working default on windows.
* `is_probably_at_fvafr()` now returns FALSE if Notebook is not connected to internal servers.
* Moved functions for CSV files to package fritools.


# frimisc 0.4.0

* Added functions for CSV files.
* Added `fake_path()`.
* `get_project_root()` now passes arguments for `get_mount_point()`.
* Fixed `get_mount_point()` for FVA net drives.

# frimisc 0.2.0

* Added data for importing.

# frimisc 0.1.0

* Added a `NEWS.md` file to track changes to the package.

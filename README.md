[![pipeline status](https://gitlab.com/fvafrcu/frimisc/badges/master/pipeline.svg)](https://gitlab.com/fvafrcu/frimisc/-/commits/master)    
[![coverage report](https://gitlab.com/fvafrcu/frimisc/badges/master/coverage.svg)](https://gitlab.com/fvafrcu/frimisc/-/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrcu/frimisc.svg?branch=master)](https://travis-ci.org/fvafrcu/frimisc)
    [![Coverage Status](https://codecov.io/github/fvafrcu/frimisc/coverage.svg?branch=master)](https://codecov.io/github/fvafrcu/frimisc?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/frimisc)](https://cran.r-project.org/package=frimisc)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/frimisc)](https://cran.r-project.org/package=frimisc)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/frimisc)](https://cran.r-project.org/package=frimisc)

<!-- README.md is generated from README.Rmd. Please edit that file -->



# frimisc
## Introduction
Please read the
[vignette](https://fvafrcu.gitlab.io/frimisc/doc/An_Introduction_to_frimisc.html).
<!-- 
[vignette](https://CRAN.R-project.org/package=frimisc/vignettes/An_Introduction_to_frimisc.html).

-->

Or, after installation, the help page:

```r
help("frimisc-package", package = "frimisc")
```

```
#> Miscellaneous Functions for the Forest Research Institute of the State
#> Baden-Wuerttemberg
#> 
#> Description:
#> 
#>      Package <https://CRAN.R-project.org/package=fritools> may not
#>      import other than core R. frimisc may.
#> 
#> Details:
#> 
#>      You will find the details in
#>      'vignette("An_Introduction_to_frimisc", package = "frimisc")'.
```

## Installation

You can install frimisc from gitlab via:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("fvafrcu/frimisc")
```



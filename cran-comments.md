Dear CRAN Team,
this is a resubmission of package 'frimisc'. I have added the following changes:

* Add `drop_columns()` and `git_compare_cleaned_csv()`.

Please upload to CRAN.
Best, Andreas Dominik

# Package frimisc 0.6.0.9000

Reporting is done by packager version 1.15.2


## Test environments
- R Under development (unstable) (2024-09-15 r87152)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 5 (daedalus)
   0 errors | 0 warnings | 2 notes
- win-builder (devel)

## Local test results
- RUnit:
    frimisc_unit_test - 1 test function, 0 errors, 0 failures in 1 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- tinytest:
    All ok, 4 results (43ms)
- Coverage by covr:
    frimisc Coverage: 8.50%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 123 lints in 400 lines of code (a ratio of 0.3075).
- cleanr:
    found 0 dreadful things about your code.
- codetools::checkUsagePackage:
    found 8 issues.
- devtools::spell_check:
    found 9 unkown words.

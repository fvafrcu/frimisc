\name{NEWS}
\title{NEWS}

\section{Changes in version 0.6.0.9000}{
\itemize{
\item Add \code{drop_columns()} and \code{git_compare_cleaned_csv()}.
}
}

\section{Changes in version 0.6.0}{
\itemize{
\item Added \code{send_mail()}.
\item Added git wrappers for \code{gert}.
\item \code{is_probaly_at_fva()} works with new notebooks.
\item \code{fake_directory()} now returns \code{tempdir()} if not running on known machines.
}
}

\section{Changes in version 0.5.0}{
\itemize{
\item Renamed argument \code{mustWork} to \code{fake_path()} to \code{must_work}.
\item \code{fake_path()} now has a working default on windows.
\item \code{is_probably_at_fvafr()} now returns FALSE if Notebook is not connected to internal servers.
\item Moved functions for CSV files to package fritools.
}
}

\section{Changes in version 0.4.0}{
\itemize{
\item Added functions for CSV files.
\item Added \code{fake_path()}.
\item \code{get_project_root()} now passes arguments for \code{get_mount_point()}.
\item Fixed \code{get_mount_point()} for FVA net drives.
}
}

\section{Changes in version 0.2.0}{
\itemize{
\item Added data for importing.
}
}

\section{Changes in version 0.1.0}{
\itemize{
\item Added a \code{NEWS.md} file to track changes to the package.
}
}

